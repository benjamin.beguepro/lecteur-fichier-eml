﻿namespace lecteur_fichiers_EML
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBxCheminAcces = new System.Windows.Forms.TextBox();
            this.btnVld = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBxFromSujet = new System.Windows.Forms.TextBox();
            this.txtBxFromTo = new System.Windows.Forms.TextBox();
            this.txtBxFrom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBxDateEnvoi = new System.Windows.Forms.TextBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtBxCheminAcces
            // 
            this.txtBxCheminAcces.Location = new System.Drawing.Point(267, 30);
            this.txtBxCheminAcces.Name = "txtBxCheminAcces";
            this.txtBxCheminAcces.Size = new System.Drawing.Size(334, 22);
            this.txtBxCheminAcces.TabIndex = 0;
            this.txtBxCheminAcces.Text = "Chemin d\'accés au fichier EML";
            // 
            // btnVld
            // 
            this.btnVld.Location = new System.Drawing.Point(670, 637);
            this.btnVld.Name = "btnVld";
            this.btnVld.Size = new System.Drawing.Size(134, 54);
            this.btnVld.TabIndex = 1;
            this.btnVld.Text = "Valider";
            this.btnVld.UseVisualStyleBackColor = true;
            this.btnVld.Click += new System.EventHandler(this.btnVld_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(267, 318);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(380, 287);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "From :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "To :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(364, 298);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Contenu (format HTML) :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sujet :";
            // 
            // txtBxFromSujet
            // 
            this.txtBxFromSujet.Location = new System.Drawing.Point(267, 216);
            this.txtBxFromSujet.Name = "txtBxFromSujet";
            this.txtBxFromSujet.Size = new System.Drawing.Size(380, 22);
            this.txtBxFromSujet.TabIndex = 7;
            // 
            // txtBxFromTo
            // 
            this.txtBxFromTo.Location = new System.Drawing.Point(267, 157);
            this.txtBxFromTo.Name = "txtBxFromTo";
            this.txtBxFromTo.Size = new System.Drawing.Size(380, 22);
            this.txtBxFromTo.TabIndex = 8;
            // 
            // txtBxFrom
            // 
            this.txtBxFrom.Location = new System.Drawing.Point(267, 91);
            this.txtBxFrom.Name = "txtBxFrom";
            this.txtBxFrom.Size = new System.Drawing.Size(380, 22);
            this.txtBxFrom.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 283);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Date d\'envoi :";
            // 
            // txtBxDateEnvoi
            // 
            this.txtBxDateEnvoi.Location = new System.Drawing.Point(267, 273);
            this.txtBxDateEnvoi.Name = "txtBxDateEnvoi";
            this.txtBxDateEnvoi.Size = new System.Drawing.Size(380, 22);
            this.txtBxDateEnvoi.TabIndex = 11;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(701, 91);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(640, 526);
            this.webBrowser1.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(923, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Contenu (interprété) :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1367, 703);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.txtBxDateEnvoi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBxFrom);
            this.Controls.Add(this.txtBxFromTo);
            this.Controls.Add(this.txtBxFromSujet);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btnVld);
            this.Controls.Add(this.txtBxCheminAcces);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBxCheminAcces;
        private System.Windows.Forms.Button btnVld;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBxFromSujet;
        private System.Windows.Forms.TextBox txtBxFromTo;
        private System.Windows.Forms.TextBox txtBxFrom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBxDateEnvoi;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Label label6;
    }
}

