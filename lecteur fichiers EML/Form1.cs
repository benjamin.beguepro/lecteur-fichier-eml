﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace lecteur_fichiers_EML
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        protected CDO.Message ReadMessage(String emlFileName)
        {
            CDO.Message msg = new CDO.MessageClass();
            ADODB.Stream stream = new ADODB.StreamClass();
            stream.Open(Type.Missing,
                           ADODB.ConnectModeEnum.adModeUnknown,
                           ADODB.StreamOpenOptionsEnum.adOpenStreamUnspecified,
                           String.Empty,
                           String.Empty);
            stream.LoadFromFile(emlFileName);
            stream.Flush();
            msg.DataSource.OpenObject(stream, "_Stream");
            msg.DataSource.Save();
            return msg;
        }

        private void btnVld_Click(object sender, EventArgs e)
        {
            string fichier2 = Application.StartupPath + "\\" + txtBxCheminAcces.Text.ToString();

            CDO.Message resul = ReadMessage(fichier2);
            richTextBox1.Text = resul.HTMLBody;
            txtBxFrom.Text = "From : " + resul.From.ToString();
            txtBxFromTo.Text = "\r\n TO :" + resul.To;
            txtBxFromSujet.Text = "\r\n Sujet :" + resul.Subject;
            txtBxDateEnvoi.Text = "\r\n date d'envoi :" + resul.SentOn;
            webBrowser1.DocumentText = resul.HTMLBody;
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }
       // Selection_jeux.eml
        //uberEat.eml
    }
}
